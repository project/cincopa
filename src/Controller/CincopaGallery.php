<?php

namespace Drupal\cincopa\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Gallery controller.
 *
 * @package Drupal\cincopa\Controller
 */
class CincopaGallery extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content(Request $request) {
    // Add new Gallery URL.
    $url = "https://www.cincopa.com/media-platform/start.aspx";
    return [
      '#theme' => 'cincopa',
      '#url' => $url,
    ];
  }

}
