<?php

namespace Drupal\cincopa\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "cincopagallery" plugin.
 *
 * @CKEditorPlugin(
 *   id = "cincopagallery",
 *   label = @Translation("Cincopa New Gallery"),
 *   module = "cincopa"
 * )
 */
class Cincopagallery extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('cincopa')
      . '/js/plugins/cincopagallery/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Cincopagallery' => [
        'label' => t('Cincopa Gallery'),
        'image' => \Drupal::service('extension.list.module')->getPath('cincopa')
        . '/js/plugins/cincopagallery/icons/cincopagallery.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    return $form;
  }

}
